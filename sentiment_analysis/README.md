# Sentiment Analysis 

Sentiment Analysis refers to the use of several techniques to systematically identify, extract, quantify, and study emotional states and subjective information. It aims to determine the attitudes, opinions, or emotional reactions of a speaker with respect to some topic. It can often be helpful in ascertaining the sentiment of a product or brand when given conversation data such as a social media feed.

# Content

`ner-sentiment-svc.py`  - Model that takes in text and processes the sentiment along with entity relationships.
##### Parameters
* text - Text to process sentiment analysis on.

# Workflow

## Save Data

Currently sentiment analysis has been implemented as an OpenWhisk action and accepts input embedded in json request. The data includes any natural language text.

## Run Model

    curl 'http://ai-library.datahub.redhat.com/api/sentiment' \
    -X POST \
    -H 'Content-Type: application/json' \
    -H 'Accept: application/json' \
    -d '{
          "text": "One of the hallmarks of Red Hat Enterprise Linux is that it overwhelmingly favors stability over currency. As such, RHEL generally ships with  packages and frameworks that are years behind the current releases. This is by design, to ensure that the RHEL distribution is as solid as possible. As an example, Red Hat's slow and steady approach saved RHEL 6.4 users from the OpenSSL Heartbleed vulnerability because all RHEL versions up to and including that version shipped with a two-year-old version of OpenSSL that was not affected.\n\nIf you follow the Fedora distribution, which serves as the icebreaker for the more stable RHEL distribution, you've seen many changes coming down the pike for RHEL 7. Many of these changes are the most fundamental we've seen in quite some time. Several are to be heralded, but others -- notably the replacement of Init and Upstart with Systemd -- are likely to chafe longtime RHEL users and potentially curb adoption.\n\n\nWhat's new in RHEL 7\nThere is a long list of changes in RHEL 7, but only a few are fundamental. RHEL 7 now uses Systemd rather than Init scripts for service startup and management -- more on that later. The new default file system is XFS rather than Ext4, with support of XFS file systems up to 500TB in size. To that end, RHEL 7 now supports Ext4 file systems as large as 50TB.\n\nLinux containers get a front-row seat in the form of Docker. RHEL can now perform cross-domain trusts with Microsoft Active Directory, so users can authenticate to Linux resources with Active Directory accounts without the need for synchronization.\n\nRHEL 7 also includes new monitoring and performance tools. For instance, the Performance Co-Pilot (PCP) provides a new API for importing, exporting, and processing performance data, while the Tuned daemon provides dynamic system performance tuning.\n\nOn the inside, RHEL 7 incorporates enhanced NUMA affinity features that optimize performance on a per-process level by aligning processor affinity to RAM location, reducing cross-node communication and improving process performance.\n\nRHEL 7 offers tighter integration with the VMware vSphere hypervisor via 3D graphics drivers for hardware acceleration with OpenGL and X11, and Open Virtual Machine Tools, an open source implementation of VMware Tools that is now a maintained package.\n\nOpen Linux Management Infrastructure (OpenLMI) is now supported. OpenLMI is a framework that allows for common configuration, management, and monitoring of hardware and software through a remote connection. It provides a standard API that can be used by any compliant controller to make changes to the server configuration or to monitor the system.\n\nOther changes include the use of Chrony versus the historical Network Time Protocol daemon for time synchronization, support for 40GB interfaces, structured logging, and low-latency sockets. A new firewall management interface, Firewalld, now permits firewall configuration changes without restarting.\n\nNone of these changes or additions will come as much of a surprise to anyone who's been working with Red Hat's Fedora distribution. But those who working exclusively within the RHEL 5 and RHEL 6 ecosystems are in for a jolt.\n\nBrace for impact \nOf the myriad changes found in RHEL 7, a few are certain to cause consternation. First and foremost of those is the move to the Systemd system and process manager. This represents a major departure from Red Hat's -- and Linux's -- history and from the tried-and-true Unix philosophy of using simple, modular tools for critical infrastructure components. Systemd replaces the simplicity of Init scripts with a major management system that offers new features and capabilities but adds significant complexity. \n\nSome of the benefits to Systemd are the parallelized service startup at boot and centralized service management -- and it certainly shortens boot times.\n\nHowever, there are decades of admin reflexes to overcome by introducing Systemd, and those tasked with maintaining servers running RHEL 6 and RHEL 7 releases will quickly tire of the significant administrative differences between them. Red Hat has replicated many original commands to Systemd commands to address this issue (see the Fedora project's SysVinit to Systemd Cheatsheet). But at the heart of the matter, an extremely fundamental part of RHEL server administration is now wildly altered.\n\nTo take one example, for 20 years we've been able to issue the chkconfig -list command to show what services are set to start and at what run level. That command is now systemctl list-unit-files --type=service. For the moment, chkconfig -list still works, but chides you for not using the systemctl call. In /etc/init.d you'll find only a few scripts and a README.\n\nBoth sides of the Systemd divide have their adherents, but in RHEL 7, the Systemd argument has clearly won. I believe, however, that this will ultimately rankle many veteran Linux admins, and we may be on the road to a real schism in the RHEL community and in the Linux world at large.\n\nSmoother sailing\nRHEL7 will integrate Docker, the Linux containers solution. Docker is built around the Linux kernel-based virtualization method that permits multiple, isolated virtual systems, or containers, to run on a single host system. Docker makes it easy to deploy applications and services inside containers and move them between host systems without requiring specific dependencies or package installations on the target host.\n\nFor example, you could create a container on an Ubuntu server that's running a Memcached service and copy that container to an RHEL server where it would run without alteration. Linux containers and Docker can also run on physical, virtual, or cloud infrastructures, generally without requiring anything more than the Docker binary installed on the host.\n\nDocker-managed containerization is a big deal for computing in general, and the quick adoption in RHEL 7 shows that Red Hat is interested in getting on the forefront of this change, rather than backing into it in a later release.\n\nDirect support for Active Directory authentication is another significant update, one that may cause more than a few environments to finally ditch NIS and existing LDAP authentication mechanisms. RHEL 7 can now function with cross-domain trusts to Microsoft Active Directory. This means that a user existing only in Active Directory can authenticate to an RHEL 7 server without requiring any synchronization of user data between the realms.\n\nThus, environments that have been maintaining multiple authentication mechanisms for their Windows and Linux infrastructures can now combine them without jumping through too many hoops. There are many shops that still run NIS on Linux, either maintaining a completely separate authentication realm, or using one of several rather funky methods of combining the two (such as identity synchronization or using a Windows server as the NIS master).\n\nThe addition of Performance Co-Pilot (PCP) should also find many supporters. PCP can collect all kinds of performance metrics on a server and make them available to any local or remote viewer, even running on other platforms. PCP can also be used to provide detailed information on application performance. Thorough use of PCP will make troubleshooting intractable server-side problems easier and offer heightened visibility into the operating state of a server.\n\nFinally, the graphical installation tool Anaconda has received a face-lift. It's much flatter, allowing all pertinent configuration elements to be set within one screen, rather than through a series of screens separated by Next buttons. Within a few clicks you can configure the system as you require, then click Install and walk away while that work is done.\n\nOn the downside, the package selection is somewhat restricting, separating certain packages by base server selection. For instance, you can't easily select MariaDB server and client in the Web server grouping, so selecting the elements of a LAMP server will need to be done after install.\n\nThat said, the new installer is clean and slick, and let's face it -- we're not likely to use the installer much these days. We'll create some templates or images and use those.\n\nRHEL 7 is a fairly significant departure from the expected full-revision release from Red Hat. This is not merely a reskinning of the previous release with updated packages, a more modern kernel, and some new toolkits and widgets. This is a very different release than RHEL 6 in any form, mostly due to the move to Systemd.\n\nThough this change has been visible for some time, it will still cause integration problems in a large number of sites with a significant RHEL installed base. You can expect the adoption of RHEL 7 to be slowed quite a bit in these places, which may push out the lifecycle of RHEL 5 and RHEL 6 longer than Red Hat may like."}'

## Use Results

    {
      "metadata": {
        "job": {
          "status": "success",
          "success": true
        }
      },
      "data": {
        "id": "9e629279a7cb4e6aa29279a7cbae6ab8",
        "entities": [
          {
            "category": [
              "ORG"
            ],
            "count": 1,
            "name": "Red Hat Enterprise Linux",
            "sentiment": {
              "neutral": 1
            }
          },
          {
            "category": [
              "ORG"
            ],
            "count": 7,
            "name": "RHEL",
            "sentiment": {
              "neutral": 5,
              "positive": 1,
              "verypositive": 1
            }
          },
          {
            "category": [
              "ORG"
            ],
            "count": 3,
            "name": "Red Hat 's",
            "sentiment": {
              "neutral": 3
            }
          },
          {
            "category": [
              "PERSON"
            ],
            "count": 1,
            "name": "RHEL 6.4",
            "sentiment": {
              "neutral": 1
            }
          },
          {
            "category": [
              "GPE",
              "PERSON"
            ],
            "count": 3,
            "name": "Fedora",
            "sentiment": {
              "neutral": 2,
              "positive": 1
            }
          },
          {
            "category": [
              "FAC",
              "ORG"
            ],
            "count": 5,
            "name": "RHEL 7",
            "sentiment": {
              "neutral": 4,
              "positive": 1
            }
          },
          {
            "category": [
              "GPE",
              "PERSON"
            ],
            "count": 2,
            "name": "Init",
            "sentiment": {
              "neutral": 2
            }
          },
          {
            "category": [
              "ORG"
            ],
            "count": 1,
            "name": "Upstart",
            "sentiment": {
              "neutral": 1
            }
          },
          {
            "category": [
              "PERSON"
            ],
            "count": 10,
            "name": "Systemd",
            "sentiment": {
              "neutral": 5,
              "positive": 5
            }
          },
          {
            "category": [
              "ORG"
            ],
            "count": 2,
            "name": "XFS",
            "sentiment": {
              "neutral": 2
            }
          },
          {
            "category": [
              "ORG"
            ],
            "count": 10,
            "name": "Linux",
            "sentiment": {
              "neutral": 8,
              "verypositive": 2
            }
          },
          {
            "category": [
              "ORG"
            ],
            "count": 7,
            "name": "Docker",
            "sentiment": {
              "neutral": 7
            }
          },
          {
            "category": [
              "ORG"
            ],
            "count": 2,
            "name": "Microsoft Active Directory",
            "sentiment": {
              "neutral": 2
            }
          },
          {
            "category": [
              "ORG"
            ],
            "count": 3,
            "name": "Active Directory",
            "sentiment": {
              "neutral": 3
            }
          },
          {
            "category": [
              "ORG"
            ],
            "count": 1,
            "name": "the Performance Co-Pilot -LRB- PCP -RRB-",
            "sentiment": {
              "positive": 1
            }
          },
          {
            "category": [
              "ORG"
            ],
            "count": 1,
            "name": "Tuned",
            "sentiment": {
              "positive": 1
            }
          },
          {
            "category": [
              "ORG"
            ],
            "count": 1,
            "name": "NUMA",
            "sentiment": {
              "neutral": 1
            }
          },
          {
            "category": [
              "ORG"
            ],
            "count": 1,
            "name": "VMware",
            "sentiment": {
              "neutral": 1
            }
          },
          {
            "category": [
              "PERSON"
            ],
            "count": 1,
            "name": "X11",
            "sentiment": {
              "neutral": 1
            }
          },
          {
            "category": [
              "ORG"
            ],
            "count": 1,
            "name": "Open Virtual Machine Tools",
            "sentiment": {
              "neutral": 1
            }
          },
          {
            "category": [
              "ORG"
            ],
            "count": 1,
            "name": "VMware Tools",
            "sentiment": {
              "neutral": 1
            }
          },
          {
            "category": [
              "ORG"
            ],
            "count": 1,
            "name": "Linux Management Infrastructure",
            "sentiment": {
              "neutral": 1
            }
          },
          {
            "category": [
              "ORG"
            ],
            "count": 1,
            "name": "Chrony",
            "sentiment": {
              "neutral": 1
            }
          },
          {
            "category": [
              "PRODUCT"
            ],
            "count": 1,
            "name": "Network Time Protocol",
            "sentiment": {
              "neutral": 1
            }
          },
          {
            "category": [
              "ORG"
            ],
            "count": 1,
            "name": "GB",
            "sentiment": {
              "neutral": 1
            }
          },
          {
            "category": [
              "PERSON"
            ],
            "count": 1,
            "name": "Firewalld",
            "sentiment": {
              "neutral": 1
            }
          },
          {
            "category": [
              "PRODUCT"
            ],
            "count": 1,
            "name": "the RHEL 5",
            "sentiment": {
              "positive": 1
            }
          },
          {
            "category": [
              "ORG",
              "LAW"
            ],
            "count": 2,
            "name": "RHEL 6",
            "sentiment": {
              "neutral": 2
            }
          },
          {
            "category": [
              "ORG"
            ],
            "count": 4,
            "name": "Red Hat",
            "sentiment": {
              "neutral": 3,
              "positive": 1
            }
          },
          {
            "category": [
              "GPE"
            ],
            "count": 1,
            "name": "SysVinit",
            "sentiment": {
              "positive": 1
            }
          },
          {
            "category": [
              "PERSON"
            ],
            "count": 1,
            "name": "Systemd Cheatsheet -RRB-",
            "sentiment": {
              "positive": 1
            }
          },
          {
            "category": [
              "ORG"
            ],
            "count": 1,
            "name": "README",
            "sentiment": {
              "neutral": 1
            }
          },
          {
            "category": [
              "ORG"
            ],
            "count": 1,
            "name": "RHEL7",
            "sentiment": {
              "neutral": 1
            }
          },
          {
            "category": [
              "GPE"
            ],
            "count": 1,
            "name": "Ubuntu",
            "sentiment": {
              "neutral": 1
            }
          },
          {
            "category": [
              "PERSON"
            ],
            "count": 1,
            "name": "Memcached",
            "sentiment": {
              "neutral": 1
            }
          },
          {
            "category": [
              "ORG"
            ],
            "count": 3,
            "name": "NIS",
            "sentiment": {
              "neutral": 3
            }
          },
          {
            "category": [
              "ORG"
            ],
            "count": 1,
            "name": "LDAP",
            "sentiment": {
              "neutral": 1
            }
          },
          {
            "category": [
              "PRODUCT"
            ],
            "count": 2,
            "name": "Windows",
            "sentiment": {
              "neutral": 2
            }
          },
          {
            "category": [
              "ORG"
            ],
            "count": 1,
            "name": "Performance Co-Pilot -LRB- PCP -RRB-",
            "sentiment": {
              "neutral": 1
            }
          },
          {
            "category": [
              "ORG"
            ],
            "count": 1,
            "name": "PCP",
            "sentiment": {
              "neutral": 1
            }
          },
          {
            "category": [
              "GPE"
            ],
            "count": 1,
            "name": "Anaconda",
            "sentiment": {
              "neutral": 1
            }
          },
          {
            "category": [
              "ORG"
            ],
            "count": 1,
            "name": "Next",
            "sentiment": {
              "positive": 1
            }
          },
          {
            "category": [
              "PERSON"
            ],
            "count": 1,
            "name": "MariaDB",
            "sentiment": {
              "neutral": 1
            }
          },
          {
            "category": [
              "ORG"
            ],
            "count": 1,
            "name": "LAMP",
            "sentiment": {
              "neutral": 1
            }
          },
          {
            "category": [
              "ORG"
            ],
            "count": 1,
            "name": "RHEL 5",
            "sentiment": {
              "neutral": 1
            }
          }
        ]
      }
    }
