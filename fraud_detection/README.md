# Fraud Detection

Fraud Detection is used to identify fradulent transactions among credit card transactions. The model is based on RandomForest regressor and returns a 1 or 0 indicating whether a given transaction is fradulent or not.

## Contents

`detect_fraud.py` - RandomeForest regressor model to identify fraudulent transaction.
##### Parameters
* s3endpointUrl - Endpoint to s3 storage backend
* s3accessKey - Access key to s3 storage backend
* s3secretKey - Secret key to s3 storage backend
* s3objectStoreLocation - Bucket name in s3 backend
* model - object key including model name (same as s3Destination above unless you relocate the model).
* s3Path - object key containing test failures to run prediction on (location in the bucket)
* s3Destination - location to store the prediction results
* input data - ":" separated value of a credit card transactions.

## Workflow

### Save Data

#### Prediction Data

The following example shows sample s3Path that points to the folder where prediction data is stored.

    s3Path - fraud_detection
    Files -
        model.pkl

The following example shows what input data looks like. 

    testfailure1.json
    {
        0.0:-1.3598071337:-0.0727811733:2.536346738:1.3781552243:-0.3383207699:0.4623877778:149.62
    }
    For examples on the columns, please refer
    https://www.kaggle.com/mlg-ulb/creditcardfraud#creditcard.csv

### Run Model

#### Prediction

    curl -v --header "Authorization: Bearer $TOKEN" \
    http://seldon-core-apiserver-panbalag.apps.cluster-dc86.dc86.openshiftworkshop.com/api/v0.1/predictions -d \
    '{"strData":"s3endpointUrl='"$S3ENDPOINTURL"', s3accessKey='"$S3ACCESSKEY"', \
      s3secretKey='"$S3SECRETKEY"', s3objectStoreLocation=DH-DEV-DATA, \
      s3Path=fraud_detection, s3Destination=fraud_detection, model=model.pkl, \
      inputdata=0.0:-1.3598071337:-0.0727811733:2.536346738:1.3781552243:-0.3383207699:0.4623877778:149.62"}' \
      -H "Content-Type: application/json"

### Use Results

#### Prediction

The following example shows what an individual prediction result looks like. The field 'ndarray' shows whether the given transaction is fraudulent or not (1.0 or 0.0).

     {
        "meta": {
         "puid": "j9qa1jjb375qrfjbb55skrd2sq",
         "tags": {
         },
         "routing": {
         },
         "requestPath": {
           "c-detectfraud": "docker.io/panbalag/fraud_detection"
         },
         "metrics": []
       },
       "data": {
         "names": [],
         "ndarray": [0.0]
      }

