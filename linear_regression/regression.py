import pandas as pd
import json
from pandas import DataFrame
from sklearn import linear_model
import csv
import argparse
import os
from os import listdir
from os.path import isfile, join
from sklearn.metrics import mean_squared_error
from math import sqrt
import statistics
import inspect
import sys
currentdir = os.path.dirname(
               os.path.abspath(
                inspect.getfile(inspect.currentframe())
                )
               )
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir + "/accuracy_measures")
import measures
sys.path.append(parentdir + "/storage")
import s3

class regression(object):

    def __init__(self):
        self.model = 'None'

    def predict(self,data,features_names):
        result = "PASS"
        params = dict((item.strip()).split("=") for item in data.split(","))
        print(params)
        eparams = ["s3Path", "s3endpointUrl", "s3objectStoreLocation",
                   "s3accessKey", "s3secretKey", "s3Destination", "training",
                   "prediction"]
        if not all (x in params for x in eparams):
          print("Not all parameters have been defined")
          result = "FAIL"
          return result
        s3Path = params['s3Path']
        s3endpointUrl = params['s3endpointUrl']
        s3objectStoreLocation = params['s3objectStoreLocation']
        s3accessKey = params['s3accessKey']
        s3secretKey = params['s3secretKey']
        s3Destination = params['s3Destination']
        training = params['training']
        prediction = params['prediction']

        if not os.path.exists('/tmp/data/'):
            os.makedirs('/tmp/data/')

        s3.download_folder(s3accessKey,
       	                   s3secretKey,
               	           s3endpointUrl,
                           s3objectStoreLocation,
                           s3Path,
                           '/tmp/data/')

        if not os.path.exists('/tmp/result/'):
            os.makedirs('/tmp/result/')

        mypath = '/tmp/data/'

        TRAINING_DATA = mypath + training

        with open(TRAINING_DATA, "rt") as f:
         reader = csv.reader(f)
         i = next(reader)

         col = i[:]
         y_col = i[-1]
         del i[-1]
         x_col = i

        data = pd.read_csv(TRAINING_DATA)
        df = DataFrame(data,columns=col)

        X = df[x_col]
        Y = df[y_col]
 
        # with sklearn
        regr = linear_model.LinearRegression()
        regr.fit(X, Y)

        print('=================\nModel Parameters:\n=================\n')
        print('----------\nIntercept:\n----------\n', regr.intercept_)
        print('-------------\nCoefficients:\n-------------\n', regr.coef_)

        # prediction with sklearn
        y_true = []
        y_pred = []
        abs_error=[]
        f = open(mypath + prediction, "r")
        lines = f.readlines()
        print('\n===========\nPrediction:\n===========\n')
        print('  Predicted   vs  Actual\n')

        outfile = open('/tmp/result/result.txt', 'a')
        output = {}
        for item in lines:
         val = item.split(',')
         expected = val[-1]
         del val[-1]
         results = list(map(float, val))
         predicted = regr.predict([results])
         y_true.append(float(expected))
         y_pred.append(float(predicted))
         print(float(predicted), ',',float(expected))

        output['predicted'] = y_pred
        output['expected'] =  y_true

        print('=========\nAccuracy: \n=========\n')
        mae = measures.predictive_accuracy(y_true,y_pred,"MAE")
        mdae = measures.predictive_accuracy(y_true,y_pred,"MdAE")
        output['mae'] = mae
        output['mdae'] = mdae
        json.dump(output, outfile)
        outfile.close()
        print(measures.predictive_accuracy(y_true,y_pred,"MAE"))
        print(measures.predictive_accuracy(y_true,y_pred,"MdAE"))

        # Write results to Ceph backend
        s3.upload_folder(s3accessKey,
                         s3secretKey,
                         s3endpointUrl,
                         s3objectStoreLocation,
                         '/tmp/result',
                         s3Destination)
        return result 


def main():
  parser = argparse.ArgumentParser()
  parser.add_argument('-data', help='prediction data set', default='')
  args = parser.parse_args()
  data = args.data
  obj = regression()
  obj.predict(data,20)
  
if __name__== "__main__":
  main()
