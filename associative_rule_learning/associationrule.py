import argparse
import model
import s3fs
import os
import csv
import psycopg2
import pyarrow.parquet as pq
import pandas as pd
import sys
import numpy as np
from collections import Counter
from itertools import combinations, groupby

class associationrule(object):

    def __init__(self):
        self.model = 'None'

    def predict(self,data,features_names):
        result = "PASS"
        params = dict((item.strip()).split("=") for item in data.split(","))
        print(params)
        eparams = ["s3Path", "s3endpointUrl", "s3objectStoreLocation",
                   "s3accessKey", "s3secretKey", "destination"]
        if not all (x in params for x in eparams):
          print("Not all parameters have been defined")
          result = "FAIL"
          return result

        s3Path = params['s3Path']
        s3endpointUrl = params['s3endpointUrl']
        s3objectStoreLocation = params['s3objectStoreLocation']
        s3accessKey = params['s3accessKey']
        s3secretKey = params['s3secretKey']
        destination = params['destination']

        clientKwargs = {'endpoint_url': s3endpointUrl}
        s3 = s3fs.S3FileSystem(secret=s3secretKey, key=s3accessKey, client_kwargs=clientKwargs)

        # read dataset from s3_bucket location 

        df_rules = pq.ParquetDataset(os.path.join(s3objectStoreLocation, s3Path), filesystem=s3).read_pandas().to_pandas()
        print("Data collected from ParquetDataset")

        pd_flat = model.data_processing(df_rules)
        rules = model.association_rules(pd_flat, 0.01)
        print("Rules generation is complete.")
        source_file = "/tmp/rules.csv"
        print("source_file:" + source_file)
        rules.to_csv(source_file)

        #Store results in Ceph backend
        destination = s3objectStoreLocation + destination
        print("destination:" + destination)

        limit = 100
        n = 0
        with s3.open(destination, "wb") as fw:
                with open("/tmp/rules.csv", "r") as fr:
                        reader = csv.reader(fr, delimiter=',')
                        for row in reader:
                                n += 1
                                print(row)	
                                fw.write(str(row).encode('utf-8'))	
                                if n == limit:
                                  break
        print("done!")
        return result
        # Upload file to the destination folder in the Ceph backend


def main():
  parser = argparse.ArgumentParser()
  parser.add_argument('-data', help='prediction data set', default='')
  args = parser.parse_args()
  data = args.data
  obj = associationrule()
  obj.predict(data,20)

if __name__== "__main__":
  main()

