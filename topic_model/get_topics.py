from urllib.request import urlopen
import argparse
import re
import nltk
from bs4 import BeautifulSoup
import model_topics as mt
import gensim
import os
from os import listdir
from os.path import isfile, join
import inspect
import sys
currentdir = os.path.dirname(
               os.path.abspath(
                inspect.getfile(inspect.currentframe())
                )
               )
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir + "/accuracy_measures")
import measures
sys.path.append(parentdir + "/storage")
import s3


NUM_TOPICS = 5
NUM_WORDS = 12
PASSES = 10

def cleanhtml(raw_html):
  cleanr = re.compile('<.*?>')
  cleantext = re.sub(cleanr, '', raw_html)
  return cleantext

def cleandoc(text):
    result=[]
    for token in gensim.utils.simple_preprocess(text) :
        if token not in gensim.parsing.preprocessing.STOPWORDS and len(token) > 3:
            result.append(token)
    return result

class get_topics(object):

    def __init__(self):
        self.model = 'None'

    def predict(self,data,features_names):
        result = "PASS"
        params = dict((item.strip()).split("=") for item in data.split(","))
        print(params)
        eparams = ["s3Path", "s3endpointUrl", "s3objectStoreLocation",
                   "s3accessKey", "s3secretKey", "s3Destination", "doc", 
                   "url"]
        if not all (x in params for x in eparams):
          print("Not all parameters have been defined")
          result = "FAIL"
          return result
        s3Path = params['s3Path']
        s3endpointUrl = params['s3endpointUrl']
        s3objectStoreLocation = params['s3objectStoreLocation']
        s3accessKey = params['s3accessKey']
        s3secretKey = params['s3secretKey']
        s3Destination = params['s3Destination']
        doc = params['doc']
        url = params['url']

        # Create S3 session to access Ceph backend and get an S3 resource
        session = s3.create_session_and_resource(s3accessKey,
                                                 s3secretKey,
                                                 s3endpointUrl)


        if url:
          f = urlopen(url)
        else:
          # Download the config file that contains hyper parameter definition
          filename = '/tmp/'+doc
          s3.download_file(session,
                           s3objectStoreLocation,
                           s3Path+"/"+doc,
                           filename)
          f = open(filename,"r")

        myfile = f.read()
        soup = BeautifulSoup(str(myfile),features="html.parser")
        clean_myfile = soup.get_text().strip()
        doc_complete = clean_myfile.split('\n')
        doc_clean = [cleandoc(doc) for doc in doc_complete]
        topics = mt.gen_topics(doc_clean, NUM_TOPICS, NUM_WORDS, PASSES)
        outfile = open('/tmp/topics.txt', 'w')
        outfile.write(topics[0])
        outfile.close()
        # Write results to Ceph backend
        s3.upload_file(session,
                       s3objectStoreLocation,
                       '/tmp/topics.txt',
                       s3Destination+"/topics.txt")
        return result


def main():
  parser = argparse.ArgumentParser()
  parser.add_argument('-data', help='prediction data set', default='')
  args = parser.parse_args()
  data = args.data
  obj = get_topics()
  output = obj.predict(data,20)
  print(output)

if __name__== "__main__":
  main()


