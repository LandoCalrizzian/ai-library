import datetime
import time
import csv
import io
import os
import boto3
import uuid
import argparse
import subprocess
import time
import json
import sys
import inspect
currentdir = os.path.dirname(
               os.path.abspath(
                inspect.getfile(inspect.currentframe())
                )
               )
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir + "/storage")
import s3

class flakes_training(object):

    def __init__(self):
        self.model = 'None'

    def predict(self,data,features_names):
        result = "PASS"
        params = dict((item.strip()).split("=") for item in data.split(","))
        print(params)
        eparams = ["s3Path", "s3endpointUrl", "s3objectStoreLocation",
                   "s3accessKey", "s3secretKey", "s3Destination"]
        if not all (x in params for x in eparams):
          print("Not all parameters have been defined")
          result = "FAIL"
          return result
        s3Path = params['s3Path']
        s3endpointUrl = params['s3endpointUrl']
        s3objectStoreLocation = params['s3objectStoreLocation']
        s3accessKey = params['s3accessKey']
        s3secretKey = params['s3secretKey']
        s3Destination = params['s3Destination']

        SOURCE_DIR = '/tmp/bots/images/'
        if not os.path.exists(SOURCE_DIR):
            os.makedirs(SOURCE_DIR)

        if os.path.exists("/tmp/bots/images/data.jsonl"):
            os.remove("/tmp/bots/images/data.jsonl")

        # Create jsonl from json training data
        fname = '/tmp/bots/images/data.jsonl'
        session = s3.create_session_and_resource(s3accessKey,
                                                 s3secretKey,
                                                 s3endpointUrl)
        objects = s3.get_objects(session, s3objectStoreLocation, s3Path)

        for key in objects:
            obj = session.Object(s3objectStoreLocation, key)
            contents = obj.get()['Body'].read().decode('utf-8')
            if contents:
                jcontents = json.loads(contents)
                with open(fname, 'a') as outfile:
                    json.dump(jcontents, outfile)
                    outfile.write('\n')

        # Train model
        cmd = "python bots/learn-tests --dry " + fname
        os.system(cmd)
        print("Training complete!")

        cmd = 'find /tmp/bots/images/ -name tests-learn*.model -printf "%f"'
        model = os.popen(cmd).read()
        print("Found model : " + model)

        print("Uploading model to: " + s3Destination)

        # Upload file to the destination folder in the Ceph backend
        s3.upload_file(session, s3objectStoreLocation,
                       "/tmp/bots/images/" + model, s3Destination)
        return result


def main():
  parser = argparse.ArgumentParser()
  parser.add_argument('-data', help='prediction data set', default='')
  args = parser.parse_args()
  data = args.data
  obj = flakes_training()
  obj.predict(data,20)
  
if __name__== "__main__":
  main()

